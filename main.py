#!/usr/bin/env python3
import browser_cookie3, os, sys, requests, cloudscraper, shutil, urllib.parse, zipfile, traceback
from bs4 import BeautifulSoup
from pathlib import Path
from ruamel.yaml import YAML
# Make ASCII colorization work on Windows
from colorama import init
init()

def main():
	"The main function. Define your variables here"
	global debug, modsUptodate, modsNew, modsUpdated, modsUnavailable, scraper
	debug = False
	modsUptodate = [0]
	modsNew = [0]
	modsUpdated = [0]
	modsUnavailable = [0]
#	cookiejar = browser_cookie3.load()
	cookiejar = browser_cookie3.firefox()
	scraper = cloudscraper.create_scraper(
		browser={
			'custom': 'Mozilla/5.0 (X11; Linux x86_64; rv:105.0) Gecko/20100101 Firefox/105.0'
		}
	)
	configFile="config.yaml"

	with open(configFile, 'r') as streamConfig:
		yaml=YAML(typ='safe')
		config = yaml.load(streamConfig)
		if debug:
			yaml.dump(config, sys.stdout)
		print(Colors.YELLOW + "If a mod is unavailable, it is only for the given mod version or something else happened" + Colors.ENDC)
		downloadedAnything = False
		for key in config:
			if config[key]['enabled']:
				downloadedAnything = True
				with open(config[key]['modList'], 'r') as streamModlist:
					modlist = yaml.load(streamModlist)
					installPath = None
					version = None
					release = None
					if 'installPath' in config[key]:
						installPath = config[key]['installPath']
					if 'version' in config[key]:
						version = config[key]['version']
					if 'release' in config[key]:
						release = config[key]['release']
					if 'gameName' in config[key]:
						gameName = config[key]['gameName']
					else:
						print("gameName not defined")
						exit(1)
					if 'downloadPath' in config[key]:
						downloadPath = config[key]['downloadPath']
					else:
						print("downloadPath not defined")
						exit(1)
					updateMods(gameName, key, version, release, downloadPath, installPath, modlist, cookiejar)
					print("")
		if not downloadedAnything:
			print(Colors.RED + "No modlists were set to 'enable: True' in " + configFile + "!" + Colors.ENDC)

def modUnavailable(modName, release):
	modsUnavailable[0] += 1
	if release == None:
		print(Colors.RED + "  Mod unavailable: " + modName + Colors.ENDC)
		return
	elif release == "latest":
		modRelease = "L"
	elif release == "alpha":
		modRelease = "A"
	elif release == "beta":
		modRelease = "B"
	elif release == "stable":
		modRelease = "R"
	else:
		print(Colors.RED + "Invalid release type: " + release + Colors.ENDC)
		exit(1)
	print(Colors.RED + "  Mod unavailable: " + modName + Colors.ENDC + "(" + modRelease + ")")

def getFilterStringFromVersion(gameName, version, cookiejar):
	# This is a curseforge filter link. They aren't deterministic, they differ per game even for the same string.
	# To get it on your own, go to CF, filter by version for the game you want and take only the ?filter.. part from the URL and use it as the variable.
	# Example of the complete version URL - https://minecraft.curseforge.com/projects/abyssalcraft/files?filter-game-version=2020709689%3A6452
	# "2020709689%3A6756" # MC 1.12.2
	try:
		filterURL = "https://legacy.curseforge.com/" + gameName
		filterURL_source_code = "N/A" # So we don't error in exception
#		response = scraper.get(filterURL)
		response = scraper.get(filterURL, cookies=cookiejar)
		filterURL_source_code = response.content
#		print(response.text)
		soup = BeautifulSoup(filterURL_source_code, "html.parser")
		fileURL = ""
		# <select id="filter-game-version" name="filter-game-version" class="select  "  >
		#   <option value="" selected="selected">All Versions</option>
		#   <option value="1738749986:68441" title="" id="filter-game-version-gameversiontype-68441">Modloader</option>
		for x in soup.find(id="filter-game-version").find_all('option'):
			if debug:
				print('filter(value): {}, version(text): {}'.format(x['value'], x.text.strip()))
			if x.text.strip() == version:
				return urllib.parse.quote(x['value'])
		print(Colors.RED + "Couldn't find filter for " + gameName + version + Colors.ENDC)
		exit(1)
	except Exception as e:
		print(Colors.RED)
		print(traceback.print_exc())
		print("Filter err on URL: " + filterURL)
		print("Source code: " + filterURL_source_code)
		print("Might be a CurseForge error, in which case update packages via pip as per the README.md!" + Colors.ENDC)
		return -1

def updateMods(gameName, entry, version, release, downloadPath, installPath, modlist, cookiejar):
	# Reset final count
	modsUptodate[0] = 0
	modsNew[0] = 0
	modsUpdated[0] = 0
	modsUnavailable[0] = 0
	historyfile = "history_" + entry + ".yaml"
	if gameName != 'Terraria':
		versionFilter = getFilterStringFromVersion(gameName, version, cookiejar)
		if versionFilter == -1:
			return -1
	if version == None:
		version = 'latest'
	gameInfo = Colors.YELLOW + "Downloading mods for " + Colors.GREEN + entry + Colors.GREEN + "(" + gameName + ")" + Colors.YELLOW + " version: " + Colors.GREEN + version + Colors.YELLOW + " taking history information from " + Colors.GREEN + \
		historyfile + Colors.YELLOW + " saving into folder " + Colors.GREEN + downloadPath + Colors.ENDC
	if installPath != None:
		gameInfo += Colors.YELLOW + " installing into folder " + Colors.GREEN + installPath + Colors.ENDC
	print(gameInfo)

	# Check if download and install folders exist
	if not os.path.isdir(downloadPath):
		print(Colors.RED + "Directory " + downloadPath + " does not exist!" + Colors.ENDC)
		exit(1)
	if installPath != None:
		if not os.path.isdir(installPath):
			print(Colors.RED + "Directory " + installPath + " does not exist!" + Colors.ENDC)
			exit(1)

	# In case historyfile does not exist, create it
	file = open(historyfile, 'a')
	file.close()

	if gameName == 'Terraria':
		# for 1.4 support we need to add Worskhop integration https://steamcommunity.com/app/1281930/workshop/
		# https://github.com/SteamRE/DepotDownloader
		print(Colors.YELLOW + "Downloading full mod list, this will take a while!" + Colors.ENDC)
		modListURL = "https://javid.ddns.net/tModLoader/DirectModDownloadListing.php"
		response = scraper.get(modListURL, cookiejar)
		modListURL_source_code = response.content
		soup = BeautifulSoup(modListURL_source_code, "html.parser")
		if response.status_code != 200:
			print(Colors.RED + "Got different statuscode than 200 from Terraria Mod Browser, maybe it's down?")
			print("URL: " + modListURL + Colors.ENDC)
			return
		for modName in modlist:
			checkModTerraria(modName, soup, downloadPath, installPath, historyfile)
	else: # CurseForge
		for modName in modlist:
			checkModCF(modName, gameName, versionFilter, release, downloadPath, installPath, historyfile, cookiejar)
	print("Mods up to date:  " + Colors.GREEN  + str(modsUptodate[0])           + Colors.ENDC)
	print("Mods new:         " + Colors.YELLOW + str(modsNew[0])                + Colors.ENDC)
	print("Mods updated:     " + Colors.YELLOW + str(modsUpdated[0]-modsNew[0]) + Colors.ENDC)
	print("Mods unavailable: " + Colors.RED    + str(modsUnavailable[0])        + Colors.ENDC)

class Colors:
	"Class for coloring output"
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'
	GREY = '\033[90m'
	RED = '\033[91m'
	GREEN = '\033[92m'
	YELLOW = '\033[93m'

def downloadMod(modName, url, version, downloadPath, historyfile, cookiejar):
	"Downloads and saves into $path+filename"
#	response = scraper.get(url)
	response = scraper.get(url, cookies=cookiejar)
	filename = response.history[-1].url.rsplit('/', 1)[-1]
	# Fix spaces and remove the apikey part with split absentbydesign-1.19-1.7.0.jar?api-key=267C6CA3
	filename = filename.replace("%20", " ").split('?')[0]
	with open(downloadPath + filename, 'wb') as file:
		file.write(response.content)
	print(Colors.YELLOW + "       Downloaded: " + filename + Colors.ENDC + " " + version)
	foundExistingEntry = False
	with open(historyfile, 'r') as streamHistory:
		yaml=YAML(typ='safe')
		history = yaml.load(streamHistory)
		if history == None: # Newly created historyfile
			history = {}
		history[modName] = {}
		history[modName]['Version'] = version
		history[modName]['File'] = filename
		history[modName]['URL'] = url
	with open(historyfile, 'w') as streamHistory:
		yaml.dump(history, streamHistory)
	return filename

def checkModCF(modName, gameName, versionFilter, release, downloadPath, installPath, historyfile, cookiejar):
	"Checks mod on CF, gameName can be wow/addons or minecraft/mc-mods for example"
	filterURL = "https://legacy.curseforge.com/" + gameName + "/" + modName + "/files/all?filter-game-version=" + versionFilter
	if debug:
		print("Current filter URL: " + filterURL)
#	response = scraper.get(filterURL)
	response = scraper.get(filterURL, cookies=cookiejar)
	filterURL_source_code = response.content
	soup = BeautifulSoup(filterURL_source_code, "html.parser")
	fileURL = ""
	if response.status_code != 200:
		modUnavailable(modName, release)
		print(Colors.RED + "Got different statuscode than 200 from mod " + modName + ", maybe the mod name changed? If so, make sure that previous files were deleted or history file is corrected for the new mod name before re-running the script.")
		print("URL: " + filterURL + Colors.ENDC)
		return
	if "This project has no files" in str(filterURL_source_code):
		modUnavailable(modName, release)
		return
	# Go through the last table, which is the table with available mod versions
	for tr in soup.findAll('tbody')[-1].findAll('tr'):
		#print(tr)
		if str(tr).find('No project files were found.') != -1:
			print("I don't think this code can be hit anymore since the /all change?")
		#	modUnavailable(modName, release)
		#	return
		else:
			try:
				versionSoup = BeautifulSoup(str(tr), "html.parser")
				# <span class="text-white">R</span>
				modRelease = versionSoup.findAll('span', {'class': 'text-white'})[0].text
				if release != 'latest':
					if modRelease == 'R' and release != 'stable':
						continue
					if modRelease == 'B' and release != 'beta':
						continue
					if modRelease == 'A' and release != 'alpha':
						continue
				# <a data-action="file-link" href="/minecraft/mc-mods/jei/files/2728940">jei_1.12.2-4.15.0.283.jar</a>
				fileURL = "https://legacy.curseforge.com" + versionSoup.findAll('a', {'data-action': 'file-link'})[0]['href']
				fileURL = fileURL.replace("/files/", "/download/") + "/file"
				versionName = versionSoup.findAll('a', {'data-action': 'file-link'})[0].text
				# Minecraft uses two modloaders - Fabric and Forge. This downloader does not support fabric since CF doesn't differentiate
				# This is a hack
				if gameName == "minecraft/mc-mods" and "-fabric-" in versionName:
					print(Colors.GREY + "       Fabric mod: " + modName + " -> skipping..." + Colors.ENDC)
					continue
				if isModUpToDate(modName, versionName, fileURL, historyfile):
					print(Colors.GREEN + "Mod is up to date: " + modName + Colors.ENDC + "(" + modRelease + ") " + versionName)
					modsUptodate[0] += 1
				else:
					print(Colors.YELLOW + "  Found newer mod: " + modName + Colors.ENDC + "(" + modRelease + ") ")
					deleteMod(modName, downloadPath, historyfile)
					filename = downloadMod(modName, fileURL, versionName, downloadPath, historyfile, cookiejar)
					if installPath != None:
						installMod(filename, installPath, downloadPath)
					modsUpdated[0] += 1
				# We found a match already, so let's stop looping through the table and move on to another mod
				return
			except Exception as e:
				print(Colors.RED + "   EXCEPTION with: " + modName)
				print(traceback.print_exc())
				print(Colors.ENDC)
				if debug:
					print(Colors.RED + "Error on URL: " + filterURL + Colors.ENDC)
					print(Colors.RED + "File URL (if we even got that far): " + fileURL + Colors.ENDC)
					print(Colors.RED + filterURL_source_code + Colors.ENDC)
				return
	# If we looped through the entire table and no files matched defined release
	# TODO - this is pretty hacky, this function should be split in two so we don't have to redownload the page
	if release == 'latest' or release == 'alpha':
		modUnavailable(modName, release)
	elif release == 'stable':
		print(Colors.GREY + "       No release: " + modName + " -> Couldn't find a stable version, trying beta..." + Colors.ENDC)
		checkModCF(modName, gameName, versionFilter, 'beta', downloadPath, installPath, historyfile, cookiejar)
	elif release == 'beta':
		print(Colors.GREY + "       No release: " + modName + " -> Couldn't find a beta version, trying alpha..." + Colors.ENDC)
		checkModCF(modName, gameName, versionFilter, 'alpha', downloadPath, installPath, historyfile, cookiejar)
	return

def checkModTerraria(modName, soup, downloadPath, installPath, historyfile, cookiejar):
	"Checks mod on tModLoader's server browser"
	gameName='Terraria'
	#http://javid.ddns.net/tModLoader/DirectModDownloadListing.php
	#Name: Early Tomes
	#Homepage:
	#Download: Click here
	#Version: v0.3.1
	#tModLoader Version: tModLoader v0.6
	foundMod = soup.find("b", string=modName)
	if not foundMod:
		modUnavailable(modName, None)
		print(Colors.RED + "Mod " + Colors.YELLOW + modName + Colors.RED + " not found, maybe you got the name wrong or it was renamed? Search for it on http://javid.ddns.net/tModLoader/DirectModDownloadListing.php")
		return
	# Mods that are currently being updated have a weird "In Progress: Name: " string in them, just catch anything that differs.
	if foundMod.findPrevious('br').nextSibling != "Name: ":
		modUnavailable(modName, None)
		print(Colors.RED + "Mod " + Colors.YELLOW + modName + Colors.RED + " is likely being updated, try later! Send a message on tModLoader's Discord #general channel if this doesn't go away." + Colors.ENDC)
		return
	#try:
	#	homepageLink = foundMod.findNext('a').contents[0]
	#	print(homepageLink)
	#except IndexError:
	#	print("No homepage!")
	fileURL = foundMod.findNext('a').findNext('a')['href']
	modVersion = foundMod.findNext('a').findNext('a').next_element.next_element.next_element[10:]
	tModLoaderVersion = foundMod.findNext('a').findNext('a').next_element.next_element.next_element.next_element.next_element[32:]
	version = modVersion + " [" + tModLoaderVersion + "]"
	try:
		if isModUpToDate(modName, version, fileURL, historyfile):
			print(Colors.GREEN + "Mod is up to date: " + modName + Colors.ENDC + " " + version)
			modsUptodate[0] += 1
		else:
			print(Colors.YELLOW + "  Found newer mod: " + modName + Colors.ENDC)
			deleteMod(modName, downloadPath, historyfile)
			filename = downloadMod(modName, fileURL, version, downloadPath, historyfile, cookiejar)
			if installPath != None:
				installMod(filename, installPath, downloadPath)
			modsUpdated[0] += 1
		# We found a match already, so let's stop looping through the table and move on to another mod
	except Exception as e:
		print(traceback.print_exc())
		print("   EXCEPTION with: " + modName + Colors.ENDC)
		print(Colors.RED + str(e))
		if debug:
			print(Colors.RED + "File URL (if we even got that far): " + fileURL + Colors.ENDC)

def installMod(filename, installPath, downloadPath):
	# If file extension is archive, de-archive and remove existing folders, otherwise just copy the file
	if filename.endswith('.zip'):
		with zipfile.ZipFile(downloadPath+filename,"r") as zip_ref:
			zipList = zip_ref.namelist()
			for archiveFile in zipList:
				# If it is a top level folder, remove it from destination
				try:
					topLevelDir = archiveFile.split('/')[0]
					if os.path.isdir(installPath + topLevelDir):
						shutil.rmtree(installPath + topLevelDir)
				except OSError as e:
					print(e)
			zip_ref.extractall(installPath)
	else:
		shutil.copyfile(downloadPath + filename, installPath + filename)
	print(Colors.YELLOW + "        Installed: " + filename + Colors.ENDC)

def isModUpToDate(modName, version, url, historyfile):
	"Checks if given mod is up to date from historyfile info"
	with open(historyfile, 'r') as streamHistory:
		yaml=YAML(typ='safe')
		history = yaml.load(streamHistory)
		if history == None: # Newly created historyfile
			return False
		for key in history:
			if key == modName:
				if "javid.ddns.net" in url: # Terraria
					if history[key]['Version'] == version:
						return True
					else:
						return False
				else: # CurseForge
					if history[key]['URL'] == url:
						return True
					else:
						return False

def deleteMod(modName, downloadPath, historyfile):
	"Deletes the old version of a mod if it exists"
	with open(historyfile, 'r') as streamHistory:
		yaml=YAML(typ='safe')
		history = yaml.load(streamHistory)
		if history == None: # Newly created historyfile
			modsNew[0] += 1
			return
		for key in history:
			if key == modName:
				modPath = downloadPath + history[key]['File']
				if os.path.isfile(modPath):
					os.remove(modPath)
					print(Colors.YELLOW + "          Removed: " + history[key]['File'] + Colors.ENDC + " " + history[key]['Version'])
					return
				else:
					print(Colors.YELLOW + "(Already) removed: " + history[key]['File'] + Colors.ENDC + " " + history[key]['Version'])
					return
	modsNew[0] += 1

if __name__ == "__main__":
	main()
