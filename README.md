![alt tag](images/example.png)

This project started as a mod downloader for Minecraft, hence the name.

Currently, it supports downloading mods for curseforge.com games, which is mostly just World of Warcraft and Minecraft, and downloading mods for Terraria from the [Mod Browser](https://github.com/tModLoader/tModLoader/wiki/Mod-Browser).

There's a proprietary Twitch client for CurseForge, but it is not available on Linux and this is a free and open source implementation.

Since CurseForge uses CloudFlare, your browser's cookies are extracted and potential CF challenges are attempted to be solved via cloudscraper.

Tested with Python 3.10 on Linux and Windows 11.

---

**Requirements:**
You need to have python, pip and git installed.

On Windows, from Powershell, you can use [Chocolatey](https://chocolatey.org/docs/installation) to install git:
```powershell
choco install -y git
```
And install [Python](https://www.python.org/downloads/windows/) while checking 'Add Python 3.x to PATH' in the installer, which will also install pip.

On Linux, use your package manager to install python, git and python-pip.

Then on either Linux or Windows, install the required libraries into your home folder:
```
pip install --user -U beautifulsoup4 browser-cookie3 cloudscraper colorama requests ruamel.yaml
```
---

**Usage:**

Download the project as .zip or clone it:
```bash
git clone https://gitlab.com/C0rn3j/MCModUpdater
# You can run 'git pull' from within this folder to update to latest version if you're using git clone
cd MCModUpdater
# Copy the example config, then edit it with your favorite text editor to fit your needs, the options are explained in the file.
cp config-example.yaml config.yaml
```
Then run the python script:
```
python3 main.py
```
You will want to create your own modlists, which just simply contain the mod name in the YAML syntax. See any of the `modlist-*.yaml` files for example.

A mod name is taken from CF URL, so for Enchanting plus, the URL is https://minecraft.curseforge.com/projects/enchanting-plus which means the modname for the text file would be "enchanting-plus" without quotes.

---

If I saved you some time, you can send me money which will be dutifully wasted on the nearby pizza/kebab store: [Donate](https://rys.pw/#donate)
